defmodule EchatWeb.ChatLive do
  use EchatWeb, :live_view
  use Phoenix.Component
  alias EchatWeb.ChatLive
  alias EchatEngine.Message

  import Ecto.Changeset

  alias EchatWeb.Router.Helpers, as: Routes
  alias EchatWeb.Endpoint

  @max_messages 1000

  defp render_message(assigns) do
    ~H"""
    <div class="message">
      <span class="author"><%= @message.author_name %>:</span>
      <%= @message.text %>
    </div>
    """
  end

  defp render_header(assigns) do
    ~H"""
    <div class="title"><b>Chat</b>
    (link share:
      <a href={Routes.live_path(Endpoint, ChatLive, @chat_id)}>
        <%= String.slice(@chat_id, 0, 20) %>...
      </a>
    )
    </div>
    """
  end

  defp render_messages(assigns) do
    ~H"""
    <h3>Messages:</h3>
    <div class="messages">
      <%= for message <- @messages do %>
        <.render_message message={message} />
      <% end %>
    </div>
    """
  end

  defp render_new_message_form(assigns) do
    ~H"""
    <div class="msg_form_wrapper">
      <.form let={f} for={@message}
        phx-submit="send" multipart={true} class="msg_form"
        onkeypress="window.submitOnEnter(event)"
        phx-change="msg_form_change"
        phx-debounce="1000" %>
      <div style="display: flex; flex-wrap: wrap">
        <div style="max-width: 20em; display: flex;">
          Name: <%= text_input f, :author_name,
                    placeholder: "(leave empty for random name)",
                    style: "height:1.8em; margin-left:1em" %>
                <%= error_tag f, :author_name %>
        </div>
        <div class="emoji-input" style="margin-left: 2em">
          <%= for emoji <- @emoji_list do %>
            <div class="emoji_button tooltip" phx-click="emoji-input" phx-value-input={emoji.unicode}>
              <%= emoji.unicode %>
              <span class="tooltiptext" style="font-size:1rem">
                <%= List.first(emoji.shortcodes) %>
              </span>
            </div>
          <% end %>
        </div>
      </div>
      <%= textarea f, :text, id: "message",
          placeholder: "Say something:",
          style: "width:100%; resize: vertical",
          autofocus: true %>
      <%= error_tag f, :text %>
			<%= hidden_input f, :chat_id, value: @chat_id %>
      <%= file_input f, :file_attachment %>
      <%= error_tag f, :file_attachment %>
      <%= submit "Post", style: "float: right" %>
    </.form>
    </div>
    """
  end

  def render(assigns) do
    ~H"""
    <.render_header chat_id={@chat_id} />
    <.render_messages messages={@messages} />
    <.render_new_message_form chat_id={@chat_id} emoji_list={@emoji_list} message={@message} />
    """
  end

  @emoji_list_length 5
  defp make_emoji_list(""), do:
    make_emoji_list("fish")

  defp make_emoji_list(string) do
    string = string
             |> String.split(" ")
             |> List.last
             |> String.replace("_", " ")
             |> String.trim()

    []
    |> try_if_not_enough(:search_by_description, string)
#    |> try_if_not_enough(:find_by_shortcode, string)
    |> try_if_not_enough(:search_by_tag, string)
  end

  defp try_if_not_enough(list, fun, string) do
    if Enum.count(list) < @emoji_list_length do
      list ++ apply(Emojix, fun, [string])
    else
      list
    end
    |> Enum.uniq()
    |> Enum.take(@emoji_list_length)
  end

  def mount(%{"id" => id}, _session, socket) do
    {:ok, _pid} = EchatEngine.connect_to_chat(id)
    messages = EchatEngine.load_messages(id)

    {:ok, assign(socket, page_title: "Chat",
      chat_id: id,
      messages: messages,
      emoji_list: make_emoji_list(""),
      message: Message.changeset(%Message{}, %{}))}
  end

  def handle_info({:message, %Message{} = message}, socket) do
    {:noreply, update(socket, :messages, fn messages ->
      Enum.take([message | messages], @max_messages)
    end)}
  end

  @text_tgt %{"_target" => ["message", "text"]}
  def handle_event("msg_form_change", @text_tgt = params, socket) do
    string = params["message"]["text"]
    emoji = make_emoji_list(string)

    {:noreply, socket
      |> update(:emoji_list, fn _ -> emoji end)
      |> update(:message, fn m ->
        Message.changeset(apply_changes(m), params["message"])
      end)
    }
  end
  
  @name_tgt %{"_target" => ["message", "author_name"]}
  def handle_event("msg_form_change", @name_tgt = params, socket) do
    string = params["message"]["author_name"]
    emoji = make_emoji_list(string)

    {:noreply, socket
      |> update(:emoji_list, fn _ -> emoji end)
      |> update(:message, fn m ->
        Message.changeset(apply_changes(m), params["message"])
      end)
    }
  end


  def handle_event("send", params, socket) do
    chat_id = params["message"]["chat_id"]
    text = params["message"]["text"]

    text = String.trim(text)

    if text != "" do
      name = params["message"]["author_name"] |> String.trim()
      name = if name == "" do
        "🐶"
      else
        name
      end

      msg = %{author_name: name, text: text}

      case EchatEngine.send_message(chat_id, msg) do
        :ok -> {:noreply, update(socket, :message, fn m ->
            Message.changeset(m, %{"text" => ""})
        end)}
        {:error, changeset} -> {:noreply, update(socket, :message, fn _m ->
            changeset
        end)}
      end
    else
      {:noreply, socket}
    end
  end

  def handle_event("emoji-input", %{"input" => emoji}, socket) do
    {:noreply, update(socket, :message, fn m ->
      change = apply_changes(m)

      Message.changeset(m, %{
        "text" => (change.text || "") <> emoji
      })
    end)}
  end
end
