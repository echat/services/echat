defmodule EchatWeb.HomeLive do
  use EchatWeb, :live_view
  use Phoenix.Component
  alias EchatWeb.ChatLive

  alias EchatWeb.Router.Helpers, as: Routes
  alias EchatWeb.Endpoint

  def render(assigns) do
    ~H"""
    <h2>Anonymous Chat</h2>
    <p>
      Press the button below to create a new room. Share the link with others to invite.
    </p>

    <button phx-click="new_room">Make new room</button>

    <div style="margin-top: 5em">
      <h2>Public Rooms</h2>
      <p>
        Below are some public rooms. Enter at your own caution!
      </p>
      <div style="display: flex; flex-direction: column">
        <%= for chat <- @chats do %>
          <a href={Routes.live_path(Endpoint, ChatLive, chat)}>
            <%= String.slice(chat, 0, 20) %>...
          </a>
        <% end %>
      </div>
    </div>  
    """
  end

  def mount(_params, _session, socket) do
    {:ok, assign(socket, :chats, EchatEngine.get_all_chats())}
  end

  def handle_event("new_room", _params, socket) do
    id = EchatEngine.new_random_chat()
    {:noreply, push_redirect(socket, to: Routes.live_path(socket, ChatLive, id))}
  end
end
